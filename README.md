## Developer Documentation

This repository is for the eLess developer documentation only. Documentation for external developers that would like to interface with the eLess application can be found at http://api.eless.de.

Each repository has its own Wiki, however, those Wikis should only be used for information that is very specific to that repository. In general you should find all the required information in this Wiki.

*How to work with this*

Click on Wiki link on the left side to get to the table of content page

*How to contribute to this documentation*

Make your edits online directly or download the repository. The documents are all saved as textfiles with the extensions .md (for markdown). There are markdown editors for your computer, that you can use, but your code editor will work just as well. Here a link to a markdown editor for Macs: https://macdown.uranusjr.com/

Here is a quick cheat sheet for the markdown synthax: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet


## Wiki features

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. The [Bitbucket documentation](https://confluence.atlassian.com/x/FA4zDQ) has more information about using a wiki.

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

Go ahead and try:

```
$ git clone https://mbaumgardt@bitbucket.org/mbaumgardt/documentation.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.

## Syntax highlighting


You can also highlight snippets of text (we use the excellent [Pygments][] library).

[Pygments]: http://pygments.org/


Here's an example of some Python code:

```
#!python

def wiki_rocks(text):
    formatter = lambda t: "funky"+t
    return formatter(text)
```


You can check out the source of this page to see how that's done, and make sure to bookmark [the vast library of Pygment lexers][lexers], we accept the 'short name' or the 'mimetype' of anything in there.
[lexers]: http://pygments.org/docs/lexers/


Have fun!
